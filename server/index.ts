import { Game } from "./lib/kernel/game";
import { Server } from "./lib/kernel/server";

const args = process.argv.slice(2);

export const game = new Game(args);
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const server = new Server();

process.on("exit", () => {
  game.shutdown();

  process.exit();
});

process.on("SIGINT", () => {
  process.exit();
});
