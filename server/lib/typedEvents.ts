import { BuildingKeyType } from "./types/types";

export interface Listener<T> {
  (event: T): void;
}

export interface Disposable {
  dispose(): void;
}

/** passes through events as they happen. You will not get events from before you start listening */
export class TypedEvent<T> {
  private listeners: Listener<T>[] = [];
  private listenersOncer: Listener<T>[] = [];

  on = (listener: Listener<T>): Disposable => {
    this.listeners.push(listener);
    return {
      dispose: () => this.off(listener),
    };
  };

  once = (listener: Listener<T>): void => {
    this.listenersOncer.push(listener);
  };

  off = (listener: Listener<T>): void => {
    const callbackIndex = this.listeners.indexOf(listener);
    if (callbackIndex > -1) this.listeners.splice(callbackIndex, 1);
  };

  emit = (event: T): void => {
    /** Update any general listeners */
    this.listeners.forEach((listener) => listener(event));

    /** Clear the `once` queue */
    if (this.listenersOncer.length > 0) {
      const toCall = this.listenersOncer;
      this.listenersOncer = [];
      toCall.forEach((listener) => listener(event));
    }
  };
}

export class TickEventType extends TypedEvent<number> {}
export class FriendRequestEventType extends TypedEvent<string> {}
export class BuildEventType extends TypedEvent<{
  factoryId: string;
  buildingKey: BuildingKeyType;
}> {}
export class CreateFactoryEventType extends TypedEvent<string> {}

export const friendRequestEvent = new FriendRequestEventType();
export const tickEvent = new TickEventType();
export const buildEvent = new BuildEventType();
export const createFactoryEvent = new CreateFactoryEventType();
