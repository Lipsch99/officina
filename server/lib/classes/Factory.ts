import {
  BuildingKeyType,
  BuildingType,
  FactoryType,
  ResourceAmount,
  ResourceKey,
  Resources,
} from "../types/types";
import { buildEvent, tickEvent } from "../typedEvents";
import { Building } from "./Building";
import { Logger } from "../logger";

export class Factory {
  private data: FactoryType;
  private buildings = new Array<Building>();

  constructor(data: FactoryType) {
    this.data = data;

    this.data.buildings.forEach((buildingData) => {
      this.buildings.push(new Building(buildingData, this));
    });

    tickEvent.on((time: number) => {
      // Logger.game(
      //   "Money: " +
      //     this.data.money +
      //     ", Wood: " +
      //     this.data.resources[ResourceKey.Wood] +
      //     ", Planks: " +
      //     this.data.resources[ResourceKey.Plank] +
      //     ", IronOre: " +
      //     this.data.resources[ResourceKey.IronOre] +
      //     ", Iron: " +
      //     this.data.resources[ResourceKey.Iron]
      // );
    });

    buildEvent.on((data) => {
      console.log(data);
      if (data.factoryId === this.data.id) {
        this.addBuilding(data.buildingKey);
      }
    });
  }

  public addBuilding(buildingKey: BuildingKeyType): void {
    Logger.game("Add new Building: " + buildingKey);

    const r = Math.random().toString(36).substring(7);

    this.buildings.push(
      new Building(
        {
          key: buildingKey,
          id: r,
        },
        this
      )
    );
  }

  public getData(): FactoryType {
    const buildingData = new Array<BuildingType>();
    this.buildings.forEach((e) => {
      buildingData.push(e.getData());
    });

    this.data.buildings = buildingData;

    return this.data;
  }

  public hasResources(resource: ResourceAmount | ResourceAmount[]): boolean {
    if (Array.isArray(resource)) {
      let returnValue = true;
      resource.forEach((res) => {
        if (!this.hasResources(res)) {
          returnValue = false;
          return;
        }
      });

      return returnValue;
    }

    return this.data.resources[resource.key] >= resource.amount;
  }

  public getResource(key: keyof Resources) {
    return this.data.resources[key];
  }

  public addResource(resource: ResourceAmount): void {
    this.data.resources[resource.key] += resource.amount;
  }

  public removeResource(resource: ResourceAmount | ResourceAmount[]): void {
    if (Array.isArray(resource)) {
      resource.forEach((res) => {
        this.removeResource(res);
      });
    } else {
      this.data.resources[resource.key] -= resource.amount;
    }
  }

  public hasMoney(amount: number): boolean {
    return this.data.money >= amount;
  }

  public addMoney(amount: number): void {
    this.data.money += amount;
  }

  public removeMoney(amount: number): void {
    this.data.money -= amount;
  }
}
