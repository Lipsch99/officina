import SimplexNoise from "simplex-noise";
import { tickEvent } from "../typedEvents";
import { Logger } from "../logger";
import { ResourceAmount, resourceTimeline } from "../types/types";
import { RessourceList } from "../data/gamedata";

class GlobalMarket {
  private buyer: { [key: string]: number } = {};
  private seller: { [key: string]: number } = {};

  private factors: { [key: string]: number } = {};

  private currentNoise = 0;
  private noiseGenerator = new SimplexNoise("1337");
  private lastResourceSave = 0;
  private ressourceSaveInterval = 5000; // time in seconds after which the new resource entry is saved

  private ressourceTimelineData: resourceTimeline = {};

  constructor() {
    tickEvent.on((time) => this.tick(time));

    RessourceList.forEach((resourceKey) => {
      this.ressourceTimelineData[resourceKey] = {};
    });
  }

  private tick(time: number) {
    // const timeSeconds = time / 5000;
    const timeSeconds = time / 1000000;
    const noise = (this.noiseGenerator.noise2D(0, timeSeconds) + 1) / 2;

    this.currentNoise = noise;

    this.updateAllFactors();

    if (time - this.lastResourceSave >= this.ressourceSaveInterval) {
      RessourceList.forEach((ressourceKey) => {
        this.ressourceTimelineData[ressourceKey][time] =
          this.factors[ressourceKey];
      });
      this.lastResourceSave = time;
    }
  }

  private factorFunction(surplus: number) {
    if (isNaN(surplus)) {
      return 0;
    }
    return 2 * (1 / (1 + Math.pow(Math.E, -surplus / 10)) - 0.5);
  }

  private updateAllFactors() {
    RessourceList.forEach((resourceKey) => {
      const currentSurplus = this.seller[resourceKey] - this.buyer[resourceKey];

      this.factors[resourceKey] =
        this.currentNoise +
        0.4 + // min 0.4, max 1.4
        this.factorFunction(currentSurplus);
    });
  }

  public getTimelineData() {
    return this.ressourceTimelineData;
  }

  public addSeller(type: string) {
    if (!this.seller[type]) {
      this.seller[type] = 0; // TODO: Initialize values based on all resources in the constructor
      this.buyer[type] = 0;
    }
    this.seller[type]++;
  }

  public addBuyer(type: string) {
    if (!this.buyer[type]) {
      this.buyer[type] = 0;
      this.seller[type] = 0;
    }
    this.buyer[type]++;
  }

  public getCurrentSpeedFactor(
    resource: ResourceAmount,
    mode: string
  ): ResourceAmount {
    const resourceKey = resource.key;
    let factor = 1;

    if (mode === "selling") {
      // if we are selling and the factor is high, we are selling lower
      factor = 1 / this.factors[resourceKey];
    } else {
      factor = this.factors[resourceKey];
    }

    resource.amount = Math.round(factor * resource.amount);

    return resource;
  }
}

export const globalMarket = new GlobalMarket();
