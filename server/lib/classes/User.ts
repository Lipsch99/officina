import { Factory } from "./Factory";
import {
  BuildingType,
  FactoryType,
  ResourcesType,
  UserType,
} from "../types/types";
import { createFactoryEvent } from "../typedEvents";
import { generateUUID } from "../utils";

export class User {
  private static instances = new Array<User>();

  public data: UserType;

  private factories = new Array<Factory>();

  constructor(data: UserType) {
    this.data = data;

    this.data.factories.forEach((factoryData) => {
      this.factories.push(new Factory(factoryData));
    });

    User.instances.push(this);

    // friendRequestEvent.on((email) => {
    //   if (email === this.data.email) {
    //     // add user to own list of friends
    //   }
    // });

    createFactoryEvent.on((email) => {
      if (email === this.data.email) {
        this.createFactory();
      }
    });
  }

  private createFactory(): void {
    this.factories.push(
      new Factory({
        id: generateUUID(),
        name: "Neue Fabrik",
        resources: {
          Wood: 0,
          Plank: 0,
          Cotton: 0,
          Cloth: 0,
          Raw_Leather: 0,
          Leather: 0,
          Crude_Oil: 0,
          Refined_Oil: 0,
          Iron_Ore: 0,
          Coal: 0,
          Iron: 0,
          Aluminum_Ore: 0,
          Aluminum: 0,
          Steel: 0,
          Chassis: 0,
          Piston: 0,
          Combustion_Engine: 0,
          Rubber: 0,
          Wheel: 0,
          Transmission: 0,
          Seat: 0,
          Sand: 0,
          Glass: 0,
          Window: 0,
          Instrument_Panel: 0,
          Car: 0,
        },
        money: 1000,
        buildings: [],
        max_buildings: 5,
      })
    );
  }

  public static findUser(email: string): User | undefined {
    return User.instances.find((user) => {
      return user.data.email === email;
    });
  }

  public getData(): UserType {
    const factoryData = new Array<FactoryType>();
    this.factories.forEach((e) => {
      factoryData.push(e.getData());
    });

    this.data.factories = factoryData;

    return this.data;
  }
}
