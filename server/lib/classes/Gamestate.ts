import { GameStateType, UserType } from "../types/types";
import { User } from "./User";
import { Logger } from "../logger";
import * as argon2 from "argon2";
import { generateUUID } from "../utils";

export class Gamestate {
  private data: GameStateType;

  private users = new Array<User>();

  constructor(data: string) {
    this.data = JSON.parse(data) as GameStateType;

    this.data.users.forEach((userData) => {
      this.users.push(new User(userData));
    });
  }

  async createUser(email: string, password: string): Promise<User> {
    const userData: UserType = {
      email: email,
      password: await argon2.hash(password),
      factories: [],
    };

    const user = new User(userData);
    this.users.push(user);

    return user;
  }

  persist(): string {
    // save data
    const userData = new Array<UserType>();
    this.users.forEach((user) => {
      userData.push(user.getData());
    });

    this.data.users = userData;

    return JSON.stringify(this.data);
  }
}
