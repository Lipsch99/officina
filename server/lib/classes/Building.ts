import {
  BuildingKeyType,
  BuildingType,
  ResourceAmount,
  ResourceKey,
} from "../types/types";
import { tickEvent } from "../typedEvents";
import { Factory } from "./Factory";
import { buildingDataType, getBuildingData } from "../data/gamedata";
import { getEfficiency, takeLastXElements } from "../utils";
import { globalMarket } from "./GlobalMarket";

export class Building {
  private data: BuildingType;
  private factory: Factory;
  private buildingData: buildingDataType;
  private workHistory = new Array<boolean>();

  private maxResourceAmount = 1000;

  constructor(data: BuildingType, factory: Factory) {
    this.data = data;
    this.factory = factory;

    this.buildingData = getBuildingData(data.key);

    if (this.buildingData.type === "Buyer") {
      globalMarket.addBuyer(this.buildingData.resource_out.key);
    } else if (this.buildingData.type === "Seller") {
      globalMarket.addSeller(this.buildingData.resource_in.key);
    }

    tickEvent.on((time: number) => {
      this.tick();
    });
  }

  public getData(): BuildingType {
    return this.data;
  }

  // this gets called when a resource is SOLD
  public removeResource(ressource: ResourceAmount): void {
    ressource = globalMarket.getCurrentSpeedFactor(ressource, "selling");

    this.factory.removeResource(ressource);
  }

  // this gets called when a resource is BOUGHT
  public addResource(ressource: ResourceAmount): void {
    ressource = globalMarket.getCurrentSpeedFactor(ressource, "buying");

    this.factory.addResource(ressource);
  }

  public tick(): void {
    let didWork = false;

    switch (this.buildingData.type) {
      case "Buyer":
        if (
          this.factory.hasMoney(this.buildingData.cost) &&
          this.factory.getResource(this.buildingData.resource_out.key) <=
            this.maxResourceAmount
        ) {
          this.addResource(this.buildingData.resource_out);
          this.factory.removeMoney(this.buildingData.cost);

          didWork = true;
        }
        break;

      case "Seller":
        if (this.factory.hasResources(this.buildingData.resource_in)) {
          this.removeResource(this.buildingData.resource_in);
          this.factory.addMoney(this.buildingData.cost);

          didWork = true;
        }
        break;

      case "Producer":
        if (
          this.factory.hasResources(this.buildingData.resource_in) &&
          this.factory.getResource(this.buildingData.resource_out.key) <=
            this.maxResourceAmount
        ) {
          this.factory.removeResource(this.buildingData.resource_in);
          this.factory.addResource(this.buildingData.resource_out);

          didWork = true;
        }
        break;
    }

    this.workHistory.push(didWork);

    // remove entries older than 10
    this.workHistory = takeLastXElements(this.workHistory, 10);

    this.data.efficiency = getEfficiency(this.workHistory);
  }
}
