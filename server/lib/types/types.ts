export interface GameStateType {
  users: UserType[];
}

export interface UserType {
  password: string;
  email: string;
  factories: FactoryType[];
}

export interface FactoryType {
  id: string;
  name: string;
  resources: ResourcesType;
  buildings: BuildingType[];
  max_buildings: number;
  money: number;
}

export interface BuildingType {
  id: string;
  key: BuildingKeyType;
  efficiency?: number;
}

export type ResourcesType = {
  [key in ResourceKey]: number;
};

export interface ResourceAmount {
  key: keyof Resources;
  amount: number;
}

export interface Resources {
  Wood: number;
  Plank: number;
  Cotton: number;
  Cloth: number;
  Raw_Leather: number;
  Leather: number;
  Crude_Oil: number;
  Refined_Oil: number;
  Iron_Ore: number;
  Coal: number;
  Iron: number;
  Aluminum_Ore: number;
  Aluminum: number;
  Steel: number;
  Chassis: number;
  Piston: number;
  Combustion_Engine: number;
  Rubber: number;
  Wheel: number;
  Transmission: number;
  Seat: number;
  Sand: number;
  Glass: number;
  Window: number;
  Instrument_Panel: number;
  Car: number;
}

export enum ResourceKey {
  Wood = "Wood",
  Plank = "Plank",
  Cotton = "Cotton",
  Cloth = "Cloth",
  Raw_Leather = "Raw_Leather",
  Leather = "Leather",
  Crude_Oil = "Crude_Oil",
  Refined_Oil = "Refined_Oil",
  Iron_Ore = "Iron_Ore",
  Coal = "Coal",
  Iron = "Iron",
  Aluminum_Ore = "Aluminum_Ore",
  Aluminum = "Aluminum",
  Steel = "Steel",
  Chassis = "Chassis",
  Piston = "Piston",
  Combustion_Engine = "Combustion_Engine",
  Rubber = "Rubber",
  Wheel = "Wheel",
  Transmission = "Transmission",
  Seat = "Seat",
  Sand = "Sand",
  Glass = "Glass",
  Window = "Window",
  Instrument_Panel = "Instrument_Panel",
  Car = "Car",
}

export enum BuildingKeyType {
  //Buyers
  B_Wood = "B_Wood",
  B_Plank = "B_Plank",
  B_Cotton = "B_Cotton",
  B_Cloth = "B_Cloth",
  B_Raw_Leather = "B_Raw_Leather",
  B_Leather = "B_Leather",
  B_Crude_Oil = "B_Crude_Oil",
  B_Refined_Oil = "B_Refined_Oil",
  B_Iron_Ore = "B_Iron_Ore",
  B_Coal = "B_Coal",
  B_Iron = "B_Iron",
  B_Aluminum_Ore = "B_Aluminum_Ore",
  B_Aluminum = "B_Aluminum",
  B_Steel = "B_Steel",
  B_Chassis = "B_Chassis",
  B_Piston = "B_Piston",
  B_Combustion_Engine = "B_Combustion_Engine",
  B_Rubber = "B_Rubber",
  B_Wheel = "B_Wheel",
  B_Transmission = "B_Transmission",
  B_Seat = "B_Seat",
  B_Sand = "B_Sand",
  B_Glass = "B_Glass",
  B_Window = "B_Window",
  B_Instrument_Panel = "B_Instrument_Panel",
  //Producers
  P_Plank = "P_Plank",
  P_Cloth = "P_Cloth",
  P_Leather = "P_Leather",
  P_Refined_Oil = "P_Refined_Oil",
  P_Iron = "P_Iron",
  P_Aluminum = "P_Aluminum",
  P_Steel = "P_Steel",
  P_Chassis = "P_Chassis",
  P_Piston = "P_Piston",
  P_Combustion_Engine = "P_Combustion_Engine",
  P_Wheel = "P_Wheel",
  P_Transmission = "P_Transmission",
  P_Seat = "P_Seat",
  P_Glass = "P_Glass",
  P_Window = "P_Window",
  P_Instrument_Panel = "P_Instrument_Panel",
  P_Car = "P_Car",
  //Sellers
  S_Plank = "S_Plank",
  S_Leather = "S_Leather",
  S_Cloth = "S_Cloth",
  S_Refined_Oil = "S_Refined_Oil",
  S_Iron = "S_Iron",
  S_Aluminum = "S_Aluminum",
  S_Steel = "S_Steel",
  S_Chassis = "S_Chassis",
  S_Piston = "S_Piston",
  S_Combustion_Engine = "S_Combustion_Engine",
  S_Wheel = "S_Wheel",
  S_Transmission = "S_Transmission",
  S_Seat = "S_Seat",
  S_Glass = "S_Glass",
  S_Window = "S_Window",
  S_Instrument_Panel = "S_Instrument_Panel",
  S_Car = "S_Car",
}

export interface BuildingBlueprint {
  key: string;
  type: BuildingType;
  in: ResourceKey;
  out: ResourceKey;
  name: string;
  units_per_minute: number;
  buy_price: number;
  sell_price: number;
}

export interface resourceTimeline {
  [key: string]: singleResourceTimeline;
}

export interface singleResourceTimeline {
  [key: number]: number;
}
