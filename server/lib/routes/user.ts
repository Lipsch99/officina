import express from "express";
import { attachUser } from "../middlewares/attachUser";

export const userRoutes = express.Router();

userRoutes.get("/:id", function (req, res) {
  res.send("User with id " + req.params.id);
});

userRoutes.post("/:id", function (req, res) {
  res.send("Update user with id " + req.params.id);
});
