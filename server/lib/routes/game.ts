import express from "express";
import { buildEvent, createFactoryEvent } from "../typedEvents";
import { BuildingKeyType } from "../types/types";
import { globalMarket } from "../classes/GlobalMarket";
import { User } from "../classes/User";

export const gameRoutes = express.Router();

gameRoutes.get("/state", function (req, res) {
  res.json({
    user: res.locals.user.getData(),
  });
});

gameRoutes.get("/resourcedata", (req, res) => {
  res.json(globalMarket.getTimelineData());
});

gameRoutes.post("/create/factory", (req, res) => {
  const user = res.locals.user as User;

  if (user.getData().factories.length <= 5) {
    createFactoryEvent.emit(user.getData().email);

    res.json({ success: true });
  } else {
    res.json({ success: false, reason: "Du hast zu viele Fabriken!" });
  }
});

gameRoutes.post("/create/building", function (req, res) {
  // TODO: Validate the request body and make sure the factory belongs to the current user
  const buildingType = req.body.buildingType as BuildingKeyType;
  const factoryId = req.body.factoryId as string;

  // TODO: Validate request parameters
  buildEvent.emit({
    factoryId: factoryId,
    buildingKey: buildingType,
  });

  res.json({
    success: true,
  });
});
