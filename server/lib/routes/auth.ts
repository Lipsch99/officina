import express from "express";
import * as argon2 from "argon2";
import * as jwt from "jsonwebtoken";
import { Logger, Severity } from "../logger";
import { User } from "../classes/User";
import { UserType } from "../types/types";
import { generateUUID } from "../utils";
import { Gamestate } from "../classes/Gamestate";
import { game } from "../../index";

export const authRoutes = express.Router();

authRoutes.post("/login", async function (req, res) {
  try {
    const email = req.body.email;
    const password = req.body.password;

    if (password && email) {
      const existingUser = User.findUser(email);

      if (existingUser) {
        const passwordCheck = await argon2.verify(
          existingUser.data.password,
          password
        );

        if (passwordCheck) {
          res.json({
            user: {
              email: existingUser.data.email,
            },
            token: generateToken(existingUser),
          });
        } else {
          res.json({
            error: "Wrong credentials",
          });
        }
      } else {
        res.json({
          error: "Wrong credentials",
        });
      }
    } else {
      res.json({
        error: "Validation failed",
      });
    }
  } catch (e) {
    Logger.server(e.message, Severity.error);
    res.json({
      error: "Internal error",
    });
  }
});

authRoutes.post("/register", async function (req, res) {
  try {
    const password = req.body.password;
    const email = req.body.email;

    if (password && email) {
      const existingUser = User.findUser(email);

      if (existingUser) {
        res.json({
          error: "Email already exists",
        });
      } else {
        const user = await game.getGamestate().createUser(email, password);

        Logger.server("User " + email + " created");

        res.json({
          user: {
            email: user.data.email,
          },
          token: generateToken(user),
        });
      }
    } else {
      res.json({
        error: "Validation failed",
      });
    }
  } catch (e) {
    Logger.server(e.message, Severity.error);
    res.json({
      error: "Internal error",
    });
  }
});

function generateToken(user: User) {
  const data = {
    email: user.data.email,
  };
  const signature = "alköj45öl_$dsa54%ç&W";
  const expiration = "6h";

  return jwt.sign({ data }, signature, { expiresIn: expiration });
}
