import { BuildingKeyType, GameStateType } from "../types/types";

export const GamestateDemoData: GameStateType = {
  users: [
    {
      email: "Andi",
      password:
        "$argon2i$v=19$m=4096,t=3,p=1$eXJxJYpm+BAOLEAN+oWkhw$FooupL+lkdHIJSA4+8r2eZn5TV4RRwLXQ7Joq80P6Uw",
      factories: [],
    },
  ],
};
