import { BuildingKeyType, ResourceAmount, ResourceKey } from "../types/types";

type buildingBuyerType = {
  type: "Buyer";
  cost: number;
  resource_out: ResourceAmount;
  building_cost: number;
};

type buildingProducerType = {
  type: "Producer";
  resource_in: ResourceAmount[];
  resource_out: ResourceAmount;
  building_cost: number;
};

type buildingSellerType = {
  type: "Seller";
  cost: number;
  resource_in: ResourceAmount;
  building_cost: number;
};

export type buildingDataType =
  | buildingBuyerType
  | buildingProducerType
  | buildingSellerType;

export type buildingsDataType = {
  [key in BuildingKeyType]:
    | buildingBuyerType
    | buildingProducerType
    | buildingSellerType;
};

export const RessourceList = [
  "Wood",
  "Plank",
  "Cotton",
  "Cloth",
  "Raw Leather",
  "Leather",
  "Crude Oil",
  "Refined Oil",
  "Iron Ore",
  "Coal",
  "Iron",
  "Aluminum Ore",
  "Aluminum",
  "Steel",
  "Chassis",
  "Piston",
  "Combustion Engine",
  "Rubber",
  "Wheel",
  "Transmission",
  "Seat",
  "Sand",
  "Glass",
  "Window",
  "Instrument Panel",
  "Car",
];

export const buildingData: buildingsDataType = {
  //Buyers
  B_Wood: {
    type: "Buyer",
    cost: 4,
    resource_out: {
      key: ResourceKey.Wood,
      amount: 1,
    },
    building_cost: 100,
  },
  B_Plank: {
    type: "Buyer",
    cost: 1.2,
    resource_out: {
      key: ResourceKey.Plank,
      amount: 1,
    },
    building_cost: 100,
  },
  B_Cotton: {
    type: "Buyer",
    cost: 3,
    resource_out: {
      key: ResourceKey.Cotton,
      amount: 1,
    },
    building_cost: 100,
  },
  B_Cloth: {
    type: "Buyer",
    cost: 7.2,
    resource_out: {
      key: ResourceKey.Cloth,
      amount: 1,
    },
    building_cost: 100,
  },
  B_Raw_Leather: {
    type: "Buyer",
    cost: 6,
    resource_out: {
      key: ResourceKey.Raw_Leather,
      amount: 1,
    },
    building_cost: 100,
  },
  B_Leather: {
    type: "Buyer",
    cost: 14.4,
    resource_out: {
      key: ResourceKey.Leather,
      amount: 1,
    },
    building_cost: 100,
  },
  B_Crude_Oil: {
    type: "Buyer",
    cost: 8,
    resource_out: {
      key: ResourceKey.Crude_Oil,
      amount: 1,
    },
    building_cost: 100,
  },
  B_Refined_Oil: {
    type: "Buyer",
    cost: 19.2,
    resource_out: {
      key: ResourceKey.Refined_Oil,
      amount: 1,
    },
    building_cost: 100,
  },
  B_Iron_Ore: {
    type: "Buyer",
    cost: 6,
    resource_out: {
      key: ResourceKey.Iron_Ore,
      amount: 1,
    },
    building_cost: 100,
  },
  B_Coal: {
    type: "Buyer",
    cost: 5,
    resource_out: {
      key: ResourceKey.Coal,
      amount: 1,
    },
    building_cost: 100,
  },
  B_Iron: {
    type: "Buyer",
    cost: 20.4,
    resource_out: {
      key: ResourceKey.Iron,
      amount: 1,
    },
    building_cost: 100,
  },
  B_Aluminum_Ore: {
    type: "Buyer",
    cost: 5,
    resource_out: {
      key: ResourceKey.Aluminum_Ore,
      amount: 1,
    },
    building_cost: 100,
  },
  B_Aluminum: {
    type: "Buyer",
    cost: 18,
    resource_out: {
      key: ResourceKey.Aluminum,
      amount: 1,
    },
    building_cost: 100,
  },
  B_Steel: {
    type: "Buyer",
    cost: 26.88,
    resource_out: {
      key: ResourceKey.Steel,
      amount: 1,
    },
    building_cost: 100,
  },
  B_Chassis: {
    type: "Buyer",
    cost: 444.96,
    resource_out: {
      key: ResourceKey.Chassis,
      amount: 1,
    },
    building_cost: 100,
  },
  B_Piston: {
    type: "Buyer",
    cost: 161.28,
    resource_out: {
      key: ResourceKey.Piston,
      amount: 1,
    },
    building_cost: 100,
  },
  B_Combustion_Engine: {
    type: "Buyer",
    cost: 1161.216,
    resource_out: {
      key: ResourceKey.Combustion_Engine,
      amount: 1,
    },
    building_cost: 100,
  },
  B_Rubber: {
    type: "Buyer",
    cost: 4,
    resource_out: {
      key: ResourceKey.Rubber,
      amount: 1,
    },
    building_cost: 100,
  },
  B_Wheel: {
    type: "Buyer",
    cost: 317.28,
    resource_out: {
      key: ResourceKey.Wheel,
      amount: 1,
    },
    building_cost: 100,
  },
  B_Transmission: {
    type: "Buyer",
    cost: 483.84,
    resource_out: {
      key: ResourceKey.Transmission,
      amount: 1,
    },
    building_cost: 100,
  },
  B_Seat: {
    type: "Buyer",
    cost: 230.4,
    resource_out: {
      key: ResourceKey.Seat,
      amount: 1,
    },
    building_cost: 100,
  },
  B_Sand: {
    type: "Buyer",
    cost: 2,
    resource_out: {
      key: ResourceKey.Sand,
      amount: 1,
    },
    building_cost: 100,
  },
  B_Glass: {
    type: "Buyer",
    cost: 4.8,
    resource_out: {
      key: ResourceKey.Glass,
      amount: 1,
    },
    building_cost: 100,
  },
  B_Window: {
    type: "Buyer",
    cost: 11.52,
    resource_out: {
      key: ResourceKey.Window,
      amount: 1,
    },
    building_cost: 100,
  },
  B_Instrument_Panel: {
    type: "Buyer",
    cost: 35.28,
    resource_out: {
      key: ResourceKey.Instrument_Panel,
      amount: 1,
    },
    building_cost: 100,
  },
  //Producers
  P_Plank: {
    type: "Producer",
    resource_in: [
      {
        key: ResourceKey.Wood,
        amount: 5,
      },
    ],
    resource_out: {
      key: ResourceKey.Plank,
      amount: 20,
    },
    building_cost: 100,
  },
  P_Cloth: {
    type: "Producer",
    resource_in: [
      {
        key: ResourceKey.Cotton,
        amount: 10,
      },
    ],
    resource_out: {
      key: ResourceKey.Cloth,
      amount: 5,
    },
    building_cost: 100,
  },
  P_Leather: {
    type: "Producer",
    resource_in: [
      {
        key: ResourceKey.Raw_Leather,
        amount: 10,
      },
    ],
    resource_out: {
      key: ResourceKey.Leather,
      amount: 5,
    },
    building_cost: 100,
  },
  P_Refined_Oil: {
    type: "Producer",
    resource_in: [
      {
        key: ResourceKey.Crude_Oil,
        amount: 10,
      },
    ],
    resource_out: {
      key: ResourceKey.Refined_Oil,
      amount: 5,
    },
    building_cost: 100,
  },
  P_Iron: {
    type: "Producer",
    resource_in: [
      {
        key: ResourceKey.Iron_Ore,
        amount: 10,
      },
      {
        key: ResourceKey.Coal,
        amount: 5,
      },
    ],
    resource_out: {
      key: ResourceKey.Iron,
      amount: 5,
    },
    building_cost: 100,
  },
  P_Aluminum: {
    type: "Producer",
    resource_in: [
      {
        key: ResourceKey.Aluminum_Ore,
        amount: 10,
      },
      {
        key: ResourceKey.Coal,
        amount: 5,
      },
    ],
    resource_out: {
      key: ResourceKey.Aluminum,
      amount: 5,
    },
    building_cost: 100,
  },
  P_Steel: {
    type: "Producer",
    resource_in: [
      {
        key: ResourceKey.Iron,
        amount: 5,
      },
      {
        key: ResourceKey.Coal,
        amount: 2,
      },
    ],
    resource_out: {
      key: ResourceKey.Steel,
      amount: 5,
    },
    building_cost: 100,
  },
  P_Chassis: {
    type: "Producer",
    resource_in: [
      {
        key: ResourceKey.Iron,
        amount: 10,
      },
      {
        key: ResourceKey.Steel,
        amount: 20,
      },
    ],
    resource_out: {
      key: ResourceKey.Chassis,
      amount: 2,
    },
    building_cost: 100,
  },
  P_Piston: {
    type: "Producer",
    resource_in: [
      {
        key: ResourceKey.Steel,
        amount: 20,
      },
    ],
    resource_out: {
      key: ResourceKey.Piston,
      amount: 4,
    },
    building_cost: 100,
  },
  P_Combustion_Engine: {
    type: "Producer",
    resource_in: [
      {
        key: ResourceKey.Piston,
        amount: 4,
      },
      {
        key: ResourceKey.Steel,
        amount: 12,
      },
    ],
    resource_out: {
      key: ResourceKey.Combustion_Engine,
      amount: 1,
    },
    building_cost: 100,
  },
  P_Wheel: {
    type: "Producer",
    resource_in: [
      {
        key: ResourceKey.Rubber,
        amount: 20,
      },
      {
        key: ResourceKey.Steel,
        amount: 10,
      },
      {
        key: ResourceKey.Aluminum,
        amount: 10,
      },
    ],
    resource_out: {
      key: ResourceKey.Wheel,
      amount: 2,
    },
    building_cost: 100,
  },
  P_Transmission: {
    type: "Producer",
    resource_in: [
      {
        key: ResourceKey.Steel,
        amount: 30,
      },
    ],
    resource_out: {
      key: ResourceKey.Transmission,
      amount: 2,
    },
    building_cost: 100,
  },
  P_Seat: {
    type: "Producer",
    resource_in: [
      {
        key: ResourceKey.Leather,
        amount: 30,
      },
      {
        key: ResourceKey.Cloth,
        amount: 20,
      },
    ],
    resource_out: {
      key: ResourceKey.Seat,
      amount: 3,
    },
    building_cost: 100,
  },
  P_Glass: {
    type: "Producer",
    resource_in: [
      {
        key: ResourceKey.Sand,
        amount: 40,
      },
    ],
    resource_out: {
      key: ResourceKey.Glass,
      amount: 20,
    },
    building_cost: 100,
  },
  P_Window: {
    type: "Producer",
    resource_in: [
      {
        key: ResourceKey.Wood,
        amount: 12,
      },
    ],
    resource_out: {
      key: ResourceKey.Window,
      amount: 6,
    },
    building_cost: 100,
  },
  P_Instrument_Panel: {
    type: "Producer",
    resource_in: [
      {
        key: ResourceKey.Plank,
        amount: 8,
      },
      {
        key: ResourceKey.Aluminum,
        amount: 6,
      },
    ],
    resource_out: {
      key: ResourceKey.Instrument_Panel,
      amount: 4,
    },
    building_cost: 100,
  },
  P_Car: {
    type: "Producer",
    resource_in: [
      {
        key: ResourceKey.Chassis,
        amount: 1,
      },
      {
        key: ResourceKey.Combustion_Engine,
        amount: 1,
      },
      {
        key: ResourceKey.Wheel,
        amount: 4,
      },
      {
        key: ResourceKey.Transmission,
        amount: 1,
      },
      {
        key: ResourceKey.Seat,
        amount: 4,
      },
      {
        key: ResourceKey.Window,
        amount: 7,
      },
      {
        key: ResourceKey.Instrument_Panel,
        amount: 1,
      },
    ],
    resource_out: {
      key: ResourceKey.Car,
      amount: 1,
    },
    building_cost: 100,
  },
  //Sellers
  S_Plank: {
    type: "Seller",
    cost: 1.2,
    resource_in: {
      key: ResourceKey.Plank,
      amount: 1,
    },
    building_cost: 100,
  },
  S_Cloth: {
    type: "Seller",
    cost: 7.2,
    resource_in: {
      key: ResourceKey.Cloth,
      amount: 1,
    },
    building_cost: 100,
  },
  S_Leather: {
    type: "Seller",
    cost: 14.4,
    resource_in: {
      key: ResourceKey.Leather,
      amount: 1,
    },
    building_cost: 100,
  },
  S_Refined_Oil: {
    type: "Seller",
    cost: 19.2,
    resource_in: {
      key: ResourceKey.Refined_Oil,
      amount: 1,
    },
    building_cost: 100,
  },
  S_Iron: {
    type: "Seller",
    cost: 20.4,
    resource_in: {
      key: ResourceKey.Iron,
      amount: 1,
    },
    building_cost: 100,
  },
  S_Aluminum: {
    type: "Seller",
    cost: 18,
    resource_in: {
      key: ResourceKey.Aluminum,
      amount: 1,
    },
    building_cost: 100,
  },
  S_Steel: {
    type: "Seller",
    cost: 26.88,
    resource_in: {
      key: ResourceKey.Steel,
      amount: 1,
    },
    building_cost: 100,
  },
  S_Chassis: {
    type: "Seller",
    cost: 444.96,
    resource_in: {
      key: ResourceKey.Chassis,
      amount: 1,
    },
    building_cost: 100,
  },
  S_Piston: {
    type: "Seller",
    cost: 161.28,
    resource_in: {
      key: ResourceKey.Piston,
      amount: 1,
    },
    building_cost: 100,
  },
  S_Combustion_Engine: {
    type: "Seller",
    cost: 1161.216,
    resource_in: {
      key: ResourceKey.Combustion_Engine,
      amount: 1,
    },
    building_cost: 100,
  },
  S_Wheel: {
    type: "Seller",
    cost: 317.28,
    resource_in: {
      key: ResourceKey.Wheel,
      amount: 1,
    },
    building_cost: 100,
  },
  S_Transmission: {
    type: "Seller",
    cost: 483.84,
    resource_in: {
      key: ResourceKey.Transmission,
      amount: 1,
    },
    building_cost: 100,
  },
  S_Seat: {
    type: "Seller",
    cost: 230.4,
    resource_in: {
      key: ResourceKey.Seat,
      amount: 1,
    },
    building_cost: 100,
  },
  S_Glass: {
    type: "Seller",
    cost: 4.8,
    resource_in: {
      key: ResourceKey.Glass,
      amount: 1,
    },
    building_cost: 100,
  },
  S_Window: {
    type: "Seller",
    cost: 11.52,
    resource_in: {
      key: ResourceKey.Window,
      amount: 1,
    },
    building_cost: 100,
  },
  S_Instrument_Panel: {
    type: "Seller",
    cost: 35.28,
    resource_in: {
      key: ResourceKey.Instrument_Panel,
      amount: 1,
    },
    building_cost: 100,
  },
  S_Car: {
    type: "Seller",
    cost: 5275.9872,
    resource_in: {
      key: ResourceKey.Car,
      amount: 1,
    },
    building_cost: 100,
  },
};

export function getBuildingData(key: BuildingKeyType): buildingDataType {
  return buildingData[key];
}
