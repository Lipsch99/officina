import { GamestateDemoData } from "../data/GamestateDemoData";
import { Gamestate } from "../classes/Gamestate";
import { Logger, Severity } from "../logger";
import { tickEvent } from "../typedEvents";
import { writeFileSync, existsSync, readFileSync } from "fs";

export class Game {
  private gameState: Gamestate;
  private saveGameName: string;

  constructor(args: string[]) {
    Logger.game("Start game");
    let demoData: string;

    this.saveGameName = "saves/save.json";

    if (args.length > 0) {
      this.saveGameName = "saves/" + args[0] + ".json";
    }

    if (existsSync(this.saveGameName)) {
      demoData = readFileSync(this.saveGameName, "utf8");
    } else {
      demoData = JSON.stringify(GamestateDemoData);
    }

    this.gameState = new Gamestate(demoData);

    setInterval(() => {
      // Logger.game("Sending tick event", Severity.info);
      tickEvent.emit(Date.now());
    }, 5000);

    setInterval(() => {
      writeFileSync(this.saveGameName, this.gameState.persist(), {
        encoding: "utf8",
      });
    }, 5000);
  }

  public getGamestate(): Gamestate {
    return this.gameState;
  }

  public shutdown(): void {
    Logger.game("saving game state...", Severity.info);

    writeFileSync(this.saveGameName, this.gameState.persist(), {
      encoding: "utf8",
    });
  }
}
