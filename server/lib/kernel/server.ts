import express from "express";
import cors from "cors";
import { isAuth } from "../middlewares/isAuth";
import { attachUser } from "../middlewares/attachUser";
import { userRoutes } from "../routes/user";
import { authRoutes } from "../routes/auth";
import { Logger } from "../logger";
import { gameRoutes } from "../routes/game";

export class Server {
  private port = 8123;
  private app = express();

  constructor() {
    Logger.server("Start Server");

    this.app.use(express.json());
    this.app.use(express.urlencoded());

    this.app.use(
      cors({
        origin: [
          "http://localhost:8080",
          "https://localhost:8080",
          "http://localhost:8081",
          "https://localhost:8081",
        ],
        credentials: true,
        exposedHeaders: ["set-cookie"],
      })
    );

    this.app.use("/user", isAuth, attachUser, userRoutes);
    this.app.use("/game", isAuth, attachUser, gameRoutes);
    this.app.use("/auth", authRoutes);

    this.app.listen(this.port, () => {
      Logger.server("server started at http://localhost:" + this.port);
    });
  }
}
