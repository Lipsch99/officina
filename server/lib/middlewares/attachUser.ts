import { NextFunction, Request, Response } from "express";
import { User } from "../classes/User";
import { Logger } from "../logger";

export async function attachUser(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    const userId = res.locals.email;

    const user = User.findUser(userId);
    if (!user) {
      Logger.server("Cannot find user " + userId);
      res.status(401).end();
    } else {
      res.locals.user = user;
      return next();
    }
  } catch (e) {
    return res.json(e).status(500).end();
  }
}
