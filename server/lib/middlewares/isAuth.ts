import * as jwt from "jsonwebtoken";
import { NextFunction, Response, Request } from "express";
import { VerifyErrors } from "jsonwebtoken";
import { Logger } from "../logger";

export function isAuth(req: Request, res: Response, next: NextFunction) {
  const token = req.headers["authorization"];

  if (token == null) {
    res.sendStatus(401);
    return res.end();
  }

  jwt.verify(
    token,
    "alköj45öl_$dsa54%ç&W",
    (err: VerifyErrors | null, token: any) => {
      if (err) {
        res.sendStatus(403);
        return res.end();
      }

      res.locals.email = token.data.email;

      next();
    }
  );
}
