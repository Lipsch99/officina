enum Color {
  Reset = "\x1b[0m",
  Bright = "\x1b[1m",
  Dim = "\x1b[2m",
  Underscore = "\x1b[4m",
  Blink = "\x1b[5m",
  Reverse = "\x1b[7m",
  Hidden = "\x1b[8m",
  FgBlack = "\x1b[30m",
  FgRed = "\x1b[31m",
  FgGreen = "\x1b[32m",
  FgYellow = "\x1b[33m",
  FgBlue = "\x1b[34m",
  FgMagenta = "\x1b[35m",
  FgCyan = "\x1b[36m",
  FgWhite = "\x1b[37m",
  BgBlack = "\x1b[40m",
  BgRed = "\x1b[41m",
  BgGreen = "\x1b[42m",
  BgYellow = "\x1b[43m",
  BgBlue = "\x1b[44m",
  BgMagenta = "\x1b[45m",
  BgCyan = "\x1b[46m",
  BgWhite = "\x1b[47m",
}

export enum Severity {
  debug = "D",
  info = "I",
  warning = "W",
  error = "E",
}

export enum Module {
  server = "server",
  database = "database",
  gameloop = "gameloop",
}

function getSeverityColor(severity: Severity) {
  switch (severity) {
    case Severity.error:
      return Color.FgRed;
    case Severity.warning:
      return Color.FgYellow;
    case Severity.info:
      return Color.FgBlue;
    case Severity.debug:
      return Color.FgWhite;
  }
}

function getModuleColor(module: Module) {
  switch (module) {
    case Module.database:
      return Color.FgCyan;
    case Module.server:
      return Color.FgBlue;
    case Module.gameloop:
      return Color.FgMagenta;
  }
}

function formatString(msg: any, module: Module, severity: Severity) {
  if (typeof msg !== "string") {
    msg = JSON.stringify(msg);
  }

  return (
    getModuleColor(module) +
    "[" +
    module +
    "]:\t\t" +
    Color.Reset +
    getSeverityColor(severity) +
    severity +
    "\t" +
    msg +
    Color.Reset
  );
}

export const Logger = {
  server: function (msg: any, severity: Severity = Severity.debug): void {
    this.log(msg, Module.server, severity);
  },
  database: function (msg: any, severity: Severity = Severity.debug): void {
    this.log(msg, Module.database, severity);
  },
  game: function (msg: any, severity: Severity = Severity.debug): void {
    this.log(msg, Module.gameloop, severity);
  },
  log: function (msg: any, module: Module, severity: Severity): void {
    console.log(formatString(msg, module, severity));
  },
};
