import { v4 as uuidv4 } from "uuid";

export function takeLastXElements<T>(arr: Array<T>, x: number): Array<T> {
  return arr.slice(Math.max(arr.length - x, 0));
}

export function getEfficiency(arr: Array<boolean>): number {
  let ones = 0;

  arr.forEach((el) => {
    ones += el ? 1 : 0;
  });

  return ones / arr.length;
}

export function generateUUID(): string {
  return uuidv4();
}
