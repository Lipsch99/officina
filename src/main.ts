import { createApp } from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import axios from "axios";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faHome, faStore, faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

library.add(faHome, faStore, faUser);

axios.defaults.baseURL = "http://localhost:8123/";

const app = createApp(App).use(router);

app.component("faicon", FontAwesomeIcon);

app.mount("#app");
