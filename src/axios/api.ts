import { ResourcesType } from "./../../server/lib/types/types";
import { BuildingKeyType } from "../../server/lib/types/types";
import axios from "axios";

export const api = {
  async createFactory(): Promise<boolean> {
    const data = await axios.post("/game/create/factory");
    return true;
  },
  async deleteFactory(factoryId: string): Promise<boolean> {
    return true;
  },
  async deleteBuilding(
    factoryId: string,
    buildingId: string
  ): Promise<boolean> {
    return true;
  },
  async addBuilding(factoryId: string, buildingType: string): Promise<boolean> {
    const data = await axios.post("/game/create/building", {
      factoryId: factoryId,
      buildingType: buildingType,
    });
    return true;
  },
  async upgradeFactory(factoryId: string): Promise<boolean> {
    return true;
  },
  //change type to Resource when it's working
  async pinResource(resource: string): Promise<boolean> {
    return true;
  },
};
