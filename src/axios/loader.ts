import { Ref, ref } from "vue";
import { GamestateDemoData } from "../../server/lib/data/GamestateDemoData";
import { resourceTimeline, UserType } from "../../server/lib/types/types";
import axios from "axios";

const userData: Ref<UserType | undefined> = ref();
let currentToken = "";
let loadTimeout: number;

// TODO: Check if we really can store the token and move this part to another place
if (localStorage.getItem("token")) {
  setToken(localStorage.getItem("token") as string);
}

loadTimeout = window.setTimeout(loadData, 100);

async function loadData() {
  window.clearTimeout(loadTimeout);
  if (currentToken) {
    const data = await axios.get("/game/state");

    if (data.status === 200) {
      userData.value = data.data.user as UserType;
    }

    const marketResponse = await axios.get("/game/resourcedata");

    if (marketResponse.status === 200) {
      marketData.value = marketResponse.data as resourceTimeline;
    }

    loadTimeout = window.setTimeout(loadData, 5000);
  } else {
    // while waiting for a token use a shorter timeout.
    loadTimeout = window.setTimeout(loadData, 100);
  }
}

export function hasToken(): boolean {
  return currentToken !== "";
}

export function setToken(token: string): void {
  currentToken = token;

  localStorage.setItem("token", token);

  axios.defaults.headers.common["Authorization"] = token;
}

export function useUserData(): Ref<UserType | undefined> {
  return userData;
}

const marketData: Ref<resourceTimeline> = ref({});

export function useMarketData(): Ref<resourceTimeline> {
  return marketData;
}
